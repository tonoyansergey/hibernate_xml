package am.egs;

import am.egs.entity.Author;
import am.egs.entity.Book;
import am.egs.entity.Person;
import am.egs.hibernate.HibernateUtil;
import org.hibernate.Session;

import java.util.HashSet;
import java.util.Set;

public class App {

    public static void main(String[] args) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Author author = Author.builder()
                .firstName("Alexandre")
                .lastName("Dumas").build();

        Book book = Book.builder()
                .name("Monte Qristo")
                .author(author)
                .publishedYear("1854")
                .price("200$").build();

        Set<Book> books = new HashSet<>();
        books.add(book);

        Person person = Person.builder()
                .firstName("Sergey")
                .lastName("Tonoyan")
                .books(books).build();

        session.save(author);
        session.save(book);
        session.save(person);

        session.getTransaction().commit();
        HibernateUtil.shutDown();

    }
}
