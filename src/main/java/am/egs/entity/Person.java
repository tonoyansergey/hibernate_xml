package am.egs.entity;

import lombok.*;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "books")
@Builder
public class Person {

    private Long id;
    private String firstName;
    private String lastName;
    private Set<Book> books;
}
