package am.egs.entity;

import lombok.*;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "persons")
@Builder
public class Book {

    private Long id;
    private Author author;
    private String name;
    private String publishedYear;
    private String price;
    private Set<Person> persons;
}
