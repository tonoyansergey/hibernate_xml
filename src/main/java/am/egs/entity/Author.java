package am.egs.entity;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class Author {

    private Long id;
    private String firstName;
    private String lastName;
}
